# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Kitsu connection",
    "author" : "Tom VIGUIER",
    "description" : "Log in to kitsu",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}
import bpy
from bpy.app.handlers import persistent
from .common.import_utils import ensure_lib
mandatory_libs = [
    "gazu",
    ]
for lib in mandatory_libs :
    has_lib = ensure_lib(lib)
    if not has_lib :
        raise Exception('Error: Could not install "' + lib + '\n Try again and if this error persists install dependencies manually')
import gazu

def update_credentials(self, context) :
    self.connected = False
    self.message = 'Click on the button to connect'

@persistent
def login_handler(self, context):
    print('connecting to kitsu..')
    bpy.ops.kitsu.connect()
    if bpy.context.preferences.addons[__name__].preferences.connected :
        print('connected')
    else :
        print('Failed to connect to kitsu')
    return None

class KITSUCONNECTION_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    url : bpy.props.StringProperty(
            name="Kitsu_Api",
            default="https://ewi.kitsu.andarta-pictures.com/api" ,
            update = update_credentials           
            )
    email : bpy.props.StringProperty(
            name = 'Email',
            subtype='NONE',
            update = update_credentials,
            default = ''
            
            )
    password : bpy.props.StringProperty(
            name = 'Password',
            subtype='PASSWORD',
            update = update_credentials,
            default = ''
            )
    connected : bpy.props.BoolProperty(default = False)
    message : bpy.props.StringProperty(default = 'Please enter your credentials')
    
class KITSUCONNECTION_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Kitsu connection"
    bl_idname = "KITSUCONNECTION_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Kitsu"

    def draw(self, context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences
        layout = self.layout
        row = layout.row()
        row.prop(addon_prefs, 'url')
        row = layout.row()
        row.prop(addon_prefs, 'email')
        row = layout.row()
        row.prop(addon_prefs, 'password')
        row = layout.row()
        if addon_prefs.connected : 
            icon = 'LINKED'
        else :
            icon = 'UNLINKED'
        row.operator('kitsu.connect', icon = icon)
        row = layout.row()
        row.label(text = addon_prefs.message)
        if addon_prefs.connected : 
            row = layout.row()
            #row.prop(addon_prefs, 'projects')



class KITSUCONNECTION_OT_CONNECT(bpy.types.Operator):
    bl_idname = "kitsu.connect"
    bl_label = "Connect to kitsu "
    
    def execute(self,context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences
        try :
            gazu.client.set_host(addon_prefs.url)
            gazu.log_in(addon_prefs.email, addon_prefs.password)
            addon_prefs.connected = True
            addon_prefs.message = 'Connected'          
        except : 
            addon_prefs.message = 'Invalid connection credentials'
            return {'CANCELLED'}
        return {'FINISHED'}

classes = [KITSUCONNECTION_AddonPref,
            KITSUCONNECTION_PT_PANEL,
            KITSUCONNECTION_OT_CONNECT  
            ]

def register():
    bpy.app.handlers.load_post.append(login_handler)
    for cls in classes :
        bpy.utils.register_class(cls)  

def unregister():
    for cls in classes :
        bpy.utils.unregister_class(cls)  
    if login_handler in bpy.app.handlers.load_post :
        bpy.app.handlers.load_post.remove(login_handler)
    
